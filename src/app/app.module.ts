import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {TableBasicExample} from './user-table/table-basic-example';
import {Routes, RouterModule} from '@angular/router';
import {AppAddUser} from './add-user/add-user';
// определение маршрутов
const appRoutes: Routes = [
  { path: 'new-user', component: AppAddUser},
  { path: '', component: TableBasicExample }

];
@NgModule({
  declarations: [
    AppComponent,
    TableBasicExample,
    AppAddUser
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
