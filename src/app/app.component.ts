import { Component} from '@angular/core';

@Component({
  selector: 'app-root',

  template: `<div>
                    <h1>База данных студентов</h1>
                    <router-outlet></router-outlet>
               </div>`
})
export class AppComponent {}
