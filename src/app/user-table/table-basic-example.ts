import {Component, OnInit} from '@angular/core';
import {HttpService} from '../http.service';
import { catchError, map, tap } from 'rxjs/operators';

import {User} from '../user';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'table-basic-example',
  template: `
    <a routerLink="/new-user">Добавить студента</a>
    <div *ngIf="users">
      <h2>Список студентов:</h2>
<!--      <ul>-->
<!--        <li *ngFor="let user of users">-->
<!--          <p>{{ user.firstname }} {{ user.lastname }} {{ user.studentnumber }}</p>-->
<!--        </li>-->
<!--      </ul>-->
      <table class="table table-striped">
        <thead>
        <tr>
          <td>Имя</td>
          <td>Фамилия</td>
          <td>Студ. билет</td>
        </tr>
        </thead>
        <tbody>
        <tr *ngFor="let user of users">
          <td>{{ user.firstname }}</td>
          <td>{{ user.lastname }} </td>
          <td>{{ user.studentnumber }}</td>
        </tr>
        </tbody>
      </table>
    </div>
  `,

  styleUrls: ['table-basic-example.css'],

  providers: [HttpService]

})

export class TableBasicExample  implements OnInit{
  title = 'rot02';
  users: User[];
  constructor(private httpService: HttpService){}
  ngOnInit() {
    this.httpService.getUsers().subscribe(data => {
      this.users = data ;
    })
  }
  links = [
    { link: 'contacts', title: 'Контакты' },
    { link: 'profile',  title: 'Профиль'  }
  ];


}
