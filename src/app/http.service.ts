import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {User} from './user';
import {log} from 'util';
import {Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';

@Injectable()
export class HttpService{
  myHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient){ }

  postData(user: User){
    console.log(user.firstname);

    const body = {firstName: user.firstname, lastName: user.lastname, studentnumber: user.studentnumber};
    console.log(user.firstname);
    console.log(body.firstName);

    return this.http.post('http://localhost:8080/data/save', body, {headers: this.myHeaders});
  }


  getUsers() {

    return this.http.get<User[]>('http://localhost:8080/data', {headers: this.myHeaders}).pipe(
      catchError(this.handleError<User[]>('getUsers', []))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }}

