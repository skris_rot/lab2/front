import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TableBasicExample} from './user-table/table-basic-example';

const routes: Routes = [ { path: 'table', component: TableBasicExample }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
