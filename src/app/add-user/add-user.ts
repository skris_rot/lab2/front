import {Component} from '@angular/core';
import {HttpService} from '../http.service';
import {User} from '../user';

@Component({
  selector: 'app-add-user',
  template: `
    <div>
      <a routerLink="/">К списку студентов</a>
      <h2>Добавление студента в базу данных</h2>
      <div class="form-group">
        <h3>Имя</h3>
        <input class="form-control" type="text" size="40" [(ngModel)]="user.firstname"/>
      </div>
      <div class="form-group">
        <div class="lbl"><h3>Фамилия</h3></div>
        <input class="form-control" type="text"  name="lastname" [(ngModel)]="user.lastname"/>
      </div>
      <div class="form-group">
        <div class="lbl"><h3>Номер студ. билета</h3></div>
        <input class="form-control" type="number" name="studentnumber"  [(ngModel)]="user.studentnumber"/>
      </div>
      <p></p>
      <div class="form-group">
        <button class="btn btn-default" (click)="submit(user)">Отправить</button>
      </div>
    </div>


    <div *ngIf="done">
      <div>Получено от сервера:</div>
      <div>Имя: {{receivedUser.firstname}}</div>
      <div>Фамилия: {{receivedUser.lastname}}</div>
      <div>№: {{receivedUser.studentnumber}}</div>
    </div>

    
  `,

  styleUrls: ['add-user.css'],
  providers: [HttpService]
})
export class AppAddUser {
  title = 'rot01';
  user: User = new User(); // данные вводимого пользователя

  receivedUser: User; // полученный пользователь
  done: boolean = false;

  constructor(private httpService: HttpService) {
  }

  submit(user: User) {
    console.log(user.firstname);
    this.httpService.postData(user)
      .subscribe(
        (data: User) => {
          this.receivedUser = data;
          this.done = true;
        },
        error => console.log(error)
      );
  }
}
